\chapter{Implementace systému}
Tato kapitola se zabývá praktickou realizací systému.
Zodpovězeny budou otázky \uv{Jak a s~pomocí čeho byl systém vytvořen?}
a budou uvedeny problémy, které bylo třeba vyřešit při implementaci.

\section{Hardwarové vybavení}
\label{Hardware}
Ke~sběru dat byl využit výše avizovaný aktigraf GENEActiv od~společnosti
Activ\-in\-sights.  Systém je připraven specificky pro zpracování dat
ve~formátu poskytovaném tímto náramkem. Parametry náramku byly
specifikovány v~kapitole~\ref{shrnutiPruzkum}.

Pro serverovou část byl použit herní notebook Acer Aspire V15 Nitro Black
Edition kvůli absenci specializovaného hardware. V~reálné implementaci systému
by bylo nutné použít dostatečně výkonný server s~adekvátním množstvím paměti,
aby byla zajištěna bezproblémová obsluha klientských požadavků.
Parametry notebooku jsou uvedeny v~příloze~\ref{NotebookParametry}.

Pro stažení dat z~náramku lze využít jakýkoliv počítač s~operačním systémem
Windows, který splňuje požadavky aplikace GENEActivPCSoftware,
viz manuál výrobce~\cite{geneactivManual}.
Nahrát a následně analyzovat data lze z~jakéhokoliv zařízení pomocí
webového prohlížeče, který přistupuje k~webovému rozhraní serveru.

V~rámci celého systému tak vyjma aktigrafu nejsou požadavky na specializované
hardwarové vybavení a lze jej provozovat s~poměrně malými náklady na
pořízení i~provoz zařízení.

\section{Implementace serverové části}
Serverová část je klíčovým bodem celého systému. Stará se o~shromáždění a
analýzu dat, celkovou správu studie, prezentování výsledků a podobně.
Jedná se také o~kritický bod bezpečnosti celého systému.

\subsection{Operační systém}
Serverová implementace byla realizována ve~virtualizovaném prostředí operačního
systému Fedora. K~virtualizaci byl použit nejprve program VMware Workstation 15,
později pak kvůli problémům se~zobrazením UI byl virtuální počítač migrován do
programu Oracle VM Virtualbox. Vše bylo spuštěno pod operačním systémem Windows~10.

Virtualizace operačního systému byla zvolena z~důvodu bezpečnosti,
potencionální útočník by neměl mít možnost poškodit hlavní operační
systém Windows ani v~případě kompromitace virtualizováného \zk{OS}.
Taktéž je díky virtualizaci možné jednoduše zálohovat celý systém.
Usnadněna je rovněž potencionální migrace systému na~jiný server,
kdy lze přenést celý obraz operačního systému a spustit jej na jiném
stroji nezávisle na~odlišnosti hardware.
Navíc lze přidělovat hardwarové prostředky dle potřeby.
Nevýhodou je pak vyšší náročnost, kdy virtualizovaný operační systém nemůže
plně využít všechny dostupné prostředky systému.
Ty je třeba rozdělit mezi hostitelský a virtualizovaný operační systém.

Operační systém Fedora byl zvolen s~ohledem na vysokou míru bezpečnosti
a~důslednou péči o~aktuálnost systému ze strany společnosti RedHat
i~ze~strany komunity uživatelů.\footnote{
	Bližší informace o~operačním systému Fedora založeném na Linuxovém jádře
	lze získat na~adrese \url{https://getfedora.org}.
	Verze 31 byla ke~dni 2.\,11.\,2019 nejaktuálnější verzí, systém byl
	po~celou dobu studie pravidelně aktualizován pro zajištění
	bezpečnosti a~stability.}
Systém podporuje množství kvalitních vývojářských nástrojů. 
S~přihlédnutím k~použitému programovacímu jazyku \texttt{Python} a 
frameworku \texttt{django} je rovněž vývoj na~Linuxových distribucích 
považován za~komfortnější v~porovnáním s~operačním systémem Windows.

\subsection{Programovací jazyk}
Hlavní programovací jazyk projektu je jazyk \texttt{Python}. 
Ten byl zvolen na~základě velkého množství kvalitních knihoven především 
pro oblast strojového učení.

Pro vyšší bezpečnost řešení nebyl v~rámci systému využit vestavěný 
systémový interpret jazyka \texttt{Python}, který řídí důležité funkce 
operačního systému Fedora, ale bylo využito virtuálního prostředí 
poskytovaného nástrojem \texttt{Conda}. 
Tento nástroj taktéž umožňuje přívětivější správu balíčků 
jazyka \texttt{Python}.\footnote{
	V~rámci práce byla použita nejaktuálnější verze nástroje \texttt{Conda}, 
	například k~3.\,11.\,2019 verze 4.7.12. 
	Více informací o~nástroji \texttt{Conda} lze získat na domovské 
	stránce \url{https://docs.conda.io/en/latest/}}

Byl použit programovací jazyk \texttt{Python} ve verzi 3.7. Ten je
v~současnosti považován za standard pro vývoj aplikací s~využitím 
strojového učení. Vhodný je taktéž pro serverovou část, neboť podporuje balíčky 
specializovaných frameworků pro vývoj webových aplikací.

\subsection{Webové frameworky}
Pro usnadnění vývoje webového rozhraní serveru je vhodné využít existující 
framework. Byly prozkoumány různé webové zdroje za účelem volby správného 
frameworku.\footnote{
	Informace brané v~potaz pro volbu frameworku vycházely 
	z~následujících webových stránek: 
	\url{https://hackr.io/blog/python-frameworks}, 
	\url{https://medium.com/fintechexplained/flask-host-your-python-machine-learning-model-on-web-b598151886d} 
	-- podrobný návod na vytvoření webového serveru pro strojové učení 
	pomocí frameworku \texttt{flask}, 
	nebo \url{https://www.djangoproject.com/start/} -- tutoriál
k~frameworku \texttt{django}.} 
Volba probíhala následně především mezi frameworky \texttt{flask} 
a \texttt{django}.

Framework \texttt{flask} je mikro-framework, který poskytuje základní 
nástroje pro tvorbu webových aplikací. Je snadno rozšiřitelný, lze do něj 
přidat všechny potřebné komponenty a zároveň nepoužít komponenty redundantní.

Framework \texttt{django} poskytuje připravené komplexní řešení, obsahuje 
nástroje pro správu všech vrstev webové aplikace od databáze a správy 
uživatelských účtů až po frontendové nástroje. 
Jedná se také o~masivně používaný framework s~kvalitní dokumentací a množstvím 
tutoriálů. Velký důraz je rovněž kladen na bezpečnost. 
Dle \zkratka{OWASP} pochází většina bezpečnostních chyb pouze ze špatné 
konfigurace uživatele \cite{OWASPdjango}. 
Z~výše uvedených důvodů byl pro implementaci zvolen framework \texttt{django}.

\subsection{Webový server}
Webový server byl spuštěn jako služba pod přihlášením uživatele bez 
administrátorských práv, aby byla zajištěna vyšší bezpečnost. 
Využita byla implementace webového serveru integrovaná v~rámci 
frameworku \texttt{django}. 
Při nasazení na reálný webový server \cite{DjangoTutorial} lze aplikaci 
integrovat s~komerčně využívanými serverovými implementacemi, 
například se server \texttt{Apache} či \texttt{NGINX}. 

Server byl zpřístupněn pouze v~rámci privátní sítě a nebyl připojen do 
internetu, jelikož se z~důvodu plynoucích z~bezpečnostních opatření nařízených
během epidemie Covid19 nemohla uskutečnit pilotní studie.

Server byl zabezpečen technologií \texttt{https}, celá komunikace probíhá po
ustanovení bezpečné relace šifrovaně. K~zajištění funkcionality protokolu
\texttt{https} je potřeba disponovat platným certifikátem podepsaným 
důvěryhodnou autoritou. V~rámci~diplomové práce tak byly vytvořeny dva 
certifikáty pomocí knihovny \texttt{OpenSSL}. První certifikát simuloval 
certifikační autoritu a umožňoval vystavovat další certifikáty.
Certifikát byl importován do úložiště důvěryhodných certifikačních autorit
testovacího systému. Druhý certifikát byl poté vystaven pro samotnou webovou
stránku a podepsán pomocí certifikátu certifikační autority. 
Následně již byla veškerá komunikace zajištěna pomocí protokolu \texttt{https}.

V~praktickém nasazení v~rámci sítě internet by pro webovou stránku bylo 
zakoupeno doménové jméno a vystaven certifikát, který by byl podepsán reálnou 
certifikační autoritou. 
Všechny tyto služby jsou ovšem zpoplatněny a v~rámci diplomové práce
nebylo třeba je provádět. Byla ovšem v~plné míře simulována a ověřena tato možnost.

\subsection{Databázový model}
Data na serveru byla uložena ve formě SQLite databáze, která je nativně 
podporována v~rámci \texttt{django} frameworku. Aby bylo možné data do databáze 
uložit, je potřeba vytvořit příslušné databázové modely. 
Například data z~aktigrafu získaná za jeden časový okamžik a popsaná tabulkou 
\ref{tab:dataformat} by byla modelována pomocí třídy jazyka Python uvedené 
ve výpisu \ref{code:DataRow}.\footnote{
	Styl formátování kódu byl přebrán ze stránky 
	\url{https://www.overleaf.com/project/5dc6c8e7ff0cbd0001062926} a bude 
	použit i v~ostatních výpisech.}
\input{code/DataRow}
V~rámci reálné implementace jsou data uložena na serveru ve formě 
\zk{CSV} souborů, není třeba ukládat do databáze jednotlivé časové záznamy. 
Je ovšem potřeba vytvořit model pro celý datový \zk{CSV} soubor 
dle výpisu \ref{code:CsvData}.
\input{code/CsvData} % TODO: Aktualizovat
Dále pak definujeme model pro subjekt, kterému data patří, a podobně pro další 
objekty, které chceme uchovat v~databázi.

\subsection{Architektura webové stránky}
Webovou stránku vytvořenou v~rámci diplomové práce můžeme rozdělit na tři logické 
celky: domovskou stránku, dashboard a administrativní stránky.

Domovská stránka představuje uživateli samotný projekt, je zde popsána 
funkcionalita a jsou uvedeny odkazy na další zdroje informací. 
Také jsou zde, tak jako na všech podstránkách, uvedeny citace na podpůrné 
materiály (ikony, šablona\dots). 
Těmto citacím, které jsou potřeba pro dodržení licenčních smluv, se bude věnovat
 mimo jiné kapitola \ref{Pravo}. 
 Domovská stránka je zobrazena na obr.\,\ref{img:homepage} v~přílohách.

Dashboard přináší přehled výsledků jednotlivých subjektů. 
Data zde nelze upravovat ale pouze prohlížet. 
Dashboard zobrazuje v~první řadě přehled všech subjektů, kde je ke konkrétním 
osobám uveden vždy jen unikátní identifikační kód. 
Tato stránka je přístupná komukoliv. 
Po kliknutí na konkrétní subjekt je zobrazena stránka s~podrobnějšími údaji. 
Na této stránce lze nalézt věk subjektu, unikátní identifikační kód,
zhodnocení poruch spánku a případnou diagnózu či další poznámky.
Dále jsou zde zobrazena zpracovaná data o~spánku z~aktigrafu, a to formou grafů.
Jelikož tato stránka s~detaily obsahuje potencionálně zneužitelné informace, 
je zabezpečena. 
Zabezpečení je pak věnována vlastní kapitola \ref{zabezpeceniDetail}. 
Datům samotným z~pohledu ochrany osobních údajů se bude dále věnovat 
kapitola \ref{OsobniUdaje}. 
Na obr.\,\ref{img:dashboard1} a obr.\,\ref{img:dashboard2} v~přílohách je 
zobrazena část stránky Dashboard, konkrétně přehled subjektů a detail 
jednoho zvoleného subjektu.

Posledním logickým celkem je administrace. Tato část umožňuje přidávat, 
upravovat a mazat subjekty a přiřazovat jim data. 
Stránka je klíčová co se týče bezpečnosti a bude jí věnována kapitola 
\ref{zabezpeceniAdministrace}.

Dále pak stránka obsahuje ještě čtvrtý logický celek, stránku věnovanou 
zpracování dat pomocí strojového učení. Ta je ovšem ve výchozím stavu skrytá
a nedostupná, zobrazena a zpřístupněna je pouze přihlášeným 
privilegovaným uživatelům systému. 
Této stránce se věnuje kapitola \ref{utils}.

Všechny webové stránky byly vytvořeny pomocí responzivního web designu, tedy
jsou renderovány odlišným způsobem v~závislosti na velikosti display. 
To umožňuje vytvořit vhodné zobrazení jak na monitoru notebooku, kdy je 
kupříkladu zobrazeno 6 sloupců subjektů na stránce Dashboard, a zároveň i na
chytrém telefonu, kdy jsou zobrazeny sloupce pouze dva. Rovněž velikost textu
či obrázků je přizpůsobována dynamicky velikosti displeje.


\subsection{Zabezpečení, uživatelé a skupiny}
Pro zajištění bezpečnosti je na serveru vedena databáze uživatelských účtů. 
Pro autentizaci je potřeba zadat správné přihlašovací jméno a heslo. 
Po úspěšné autentizaci je uživateli vygenerováno \texttt{sessionId} a uloženo 
\texttt{cookies} do jeho webového prohlížeče. 
Není tedy třeba provádět přihlášení pro každou operaci. 
Implementace je řešena nativně frameworkem \texttt{django}, bezpečnost je 
ověřena a otestována řadou vývojářů a~uživatelů.

Heslo je v~databázi uloženo ve formě hashe. Je použita hashovací funkce SHA-256 
a technika \uv{solení hashe}. 
Na výpise \ref{code:hash} je demonstrováno uložení hesla s~reálnými daty, 
a je rovněž demonstrována aktualizace hashe po změně hesla.

\input{code/Hash}

Při tvorbě hesla jsou aplikovány validace bezpečnosti hesla, které lze 
dále nastavit a ještě více zpřísnit. 
Praktická ukázka je zobrazena na obr.\,\ref{img:passValid}, kde byla snaha 
nastavit uživateli \texttt{researcher1} heslo \texttt{Researcher}. 
Uživatel tak nemůže jednoduše kompromitovat bezpečnost tím, že by si 
přenastavil heslo vlastním slabým heslem, je donucen dodržet bezpečnostní 
politiku systému.

\begin{figure}[!h]
	\begin{center}
		\includegraphics[scale=0.65]{img/PasswordValidation.png}
	\end{center}
	\caption{Validace bezpečnostních politik hesla}
	\label{img:passValid}
\end{figure}

Rovněž byly vytvořeny skupiny uživatelů a těmto skupinám byla přiřazena různá 
práva (možné je také přiřadit práva přímo konkrétnímu uživateli). 
Dle tab.\,\ref{tab:raci}, kde byly definovány povinnosti a zodpovědnosti 
jednotlivých procesních rolí, byly vytvořeny tři skupiny uživatelů.

První je skupina administrátorská, kam spadá role správce serveru a role 
koordinátora studie. Uživatelé patřící do této skupiny mají plná práva pracovat 
s~daty a rovněž spravovat uživatelské účty a skupiny samotné. 
Mají také právo nahlížet do dat všech subjektů.

Druhá skupina je skupina výzkumných pracovníků. 
To jsou pověřené kontaktní osoby, které shromažďují data a mohou je 
nahrávat na server. 
Mají tedy možnost spravovat subjekty studie a jejich data. 
Tato skupina ovšem nemá práva spravovat uživatelské účty a skupiny. 
Rovněž nemají možnost prohlížet výsledky studie jednotlivých 
subjektů, viz podkapitola \ref{zabezpeceniDetail}.

Třetí skupina, subjekty studie, pak má možnost navštívit stránku dashboardu, 
kde se dozví své výsledky díky znalosti specifického kódu subjektu, 
který jim patří, a znalosti svého hesla. 
Subjekty v~rámci administrace webové stránky nemají žádné pravomoci. 
Rovněž jim není povoleno prohlížet data ostatních subjektů. 
Tím jsou naplněny požadavky dle RACI matice \ref{tab:raci}.

V~systému je tedy při správném fungování zajištěna dodatečná bezpečnost tím,
že jsou vhodně rozděleny pravomoci. Administrátor systému může 
provádět administrativní úkony a zpracovávat data.
Zároveň ovšem o~samotných subjektech nemá informace, na základě kterých by 
je mohl identifikovat, neboť jsou data pseudonymizována.
Informacemi o~identitě subjektu naopak disponuje výzkumný pracovník, typicky
ošetřující lékař. Ten je schopný uživateli v~systému vytvořit psedonymizovaný
účet, vybavit jej aktigrafem a zajistit data. 
Není ovšem schopný nahlížet do dat subjektů která byla zpracována pomocí 
strojového učení (po změně hesla uživatelem). 
Ta jsou dostupná pouze samotnému 
uživateli, kterému přísluší, či případně administrátorovi
systému, který k~datům přistupuje z~důvodu administrace či vědeckého výzkumu. 

V~rámci budoucí práce lze bezpečnost dále zvýšit dle zdroje 
\cite{DjangoAdminTips}.

\subsection{Zabezpečení stránky administrace}
\label{zabezpeceniAdministrace}
Pro přístup do Administrace je potřeba se autentizovat uživatelským jménem 
a~heslem, jak bylo popsáno výše. 
Následně je zde možnost upravovat nastavení dle nastavených práv a 
spravovat datovou základnu studie. Uživatelé, skupiny, uživatelská práva, 
subjekty, vše bylo v~podstatě vytvořeno skrze administraci. 
Jedná se tedy o~klíčovou funkcionalitu celé webové stránky.

Celá stránka je vytvořena standardizovaně skrze framework \texttt{django}. 
Díky tomu je zajištěna vysoká míra stability a bezpečnosti. 
\texttt{Django} zajišťuje například ochranu proti \zkratka{CSS}, 
proti \zkratka{CSRF} a dalším \cite{DjangoSecurity}.


\subsection{Zabezpečený přenos souboru}
Data subjektů lze nahrát přes stránku administrace, která je dostupná pouze 
po přihlášení. Tak je zajištěno, že data budou nahrávat pouze oprávněné osoby. 
Data jsou v~databázi spojena se subjektem, kterému patří. 
V~databázi ovšem není fyzicky uložen soubor, pouze cesta k~němu. 
Soubor se fyzicky zapisuje do složky \texttt{data}. 
K~tomuto souboru se následně přistupuje při dalším zpracování. 
Práva k~souboru má pouze majitel procesu, pod kterým je spuštěný webový server, 
což dále snižuje riziko zneužití. 
Při nahrávání se validuje typ, povoleny jsou pouze soubory s~příponou csv. 
Validátor frameworku \texttt{django} by měl zabránit nahrání jiných 
potencionálně škodlivých souborů na server.

\subsection{Zabezpečení stránky detaily subjektu}
\label{zabezpeceniDetail}
Stránky s~detaily subjektů byly z~důvodu zachování anonymity zabezpečeny tak, 
aby byly přístupné pouze subjektům a administrátorům. 
Subjektům je zřízen účet se stejným uživatelským jménem, jako je kód subjektu, 
a bezpečným způsobem jim musí být předáno heslo. 
Po kliknutí na detail subjektu je uživatel přesměrován na přihlašovací 
stránku a posléze je mu přístup povolen či zamítnut. 
Účet může vytvořit pouze uživatel ze skupiny administrátorů. 
To jsou také jediní další uživatelé, kteří mohou stránky navštívit, 
aby ověřili data zde uvedená a zajistili případné opravy. 
Taktéž jsou tyto události logovány, tedy lze zpětně dohledat, 
kdo s~jakým uživatelským jménem si kdy prohlížel která uživatelská data.

Původně byl v~rámci implementace zpracování dat pomocí strojového učení 
generován graf, který znázorňoval informace o~spánku subjektu, uložen jako 
obrázek. Z~tohoto obrázku se poté graf načítal na webovou stránku detailu 
subjektu. Toto řešení ovšem představovalo bezpečnostní riziko, neboť se znalostí
příslušné URL adresy bylo možné k~obrázku přistoupit přímo, a tím získat 
potencionálně citlivé informace.
Z~toho důvodu byla následně použita jiná varianta. Grafy jsou generovány 
dynamicky až po dotazu na příslušnou stránku, nejsou nikde uloženy, aby je 
nebylo možné zcizit, a jsou zobrazeny pomocí interaktivního doplňku knihovny
\texttt{plotly}. Graf je zobrazen pomocí \texttt{javascriptu}, umožňuje provádět
interaktivní podrobnou analýzu.

\subsection{Logování}
V~rámci zajištění bezpečnosti je potřebné zajistit dostatečné logování událostí.
Logování je také užitečné pro hledání chyb a podobně. 
Proto byla logování věnována patřičná pozornost.

Samotná úroveň logování závisí od toho, zda se nacházíme
v~globálním \texttt{django} nastavení \texttt{debug}. 
Pokud je režim \texttt{debug} zapnutý, je logováno více informací, které slouží 
především pro vývojáře. 
V~reálném prostředí ovšem musí být parametr \texttt{debug} nastaven na nepravdu 
a následně probíhá pouze logování patřičných událostí.

Logování používá tři výstupy. 
Logování do konzole, logování do souboru a v~případě chyby či výjimky 
odeslání emailu na email administrátora. 
Tak je zajištěna potřebná dokumentace o~činnosti systému a je zajištěna 
co nejrychlejší reakce ze strany administrátora v~případě nefunkčnosti či chyby.


\subsection{Automatické testování}
\label{AutoTests}
Automatické testování probíhalo formou unit testů. 
Zajímavostí v~případě fra\-me\-wor\-ku \texttt{django} je i fakt, 
že lze testovat třídy \texttt{view}, tedy lze pomocí unit testů prověřit 
funkcionalitu tak, jak ji vidí koncový uživatel, a zároveň je pomocí 
klasických unit testů možno ověřit kvalitu kódu.

Implementace obsahuje automatické testy především pro část zpracování dat.
Je testována konzistence a správný formát dat. Formou TDD -- Test Driven 
Development -- pak probíhala implementace některých částí systému.

Automatické testy pro konzistenci dat byly zpřístupněny přes UI, 
neboť jsou extrémně užitečné v~procesu zpracování dat, viz kap.\,\ref{utils}.


\subsection{Zdrojové kódy}
Zdrojové kódy projektu lze nalézt na následujícím 
odkaze: \url{https://gitlab.com/MarekMikulec/geneactiv-processing-data}. 
Na odkaze naleznete poslední stabilní verzi, která je obsažena ve vývojové 
větvi \texttt{master}. 
Pro možnost prezentovat zdrojový kód v~podobě, v~jaké se nacházel v~době 
odevzdání diplomové práce, byla vytvořena vývojová větev 
\texttt{diploma-thesis} dostupná na odkaze 
\url{https://gitlab.com/MarekMikulec/geneactiv-processing-data/tree/diploma-thesis}. 
Zde lze ověřit, že poslední změny pochází z~doby před termínem odevzdání práce.

\section{Klientská část systému}
V~rámci klientské části z~pohledu výzkumného pracovníka není třeba další 
implementace. 
Jak bylo řečeno v~rámci podkapitoly \ref{Hardware}, je vyžadován jakýkoliv 
počítač, který je kompatibilní s~GENEActivPcSoftware, 
pro stažení dat z~aktigrafu, tedy v~podstatě libovolný počítač s~rozhraním USB 
a operačním systémem Windows verze 7 a výše. 
Dále je pak vyžadován standardní webový prohlížeč kompatibilní se značkovacím 
jazykem HTML5. 
Potřebná je pouze znalost příslušné webové stránky a přihlašovacích údajů.

Z~pohledu klienta jako subjektu studie je vyžadováno jakékoliv zařízení 
s~webovým prohlížečem kompatibilní se značkovacím jazykem HTML5. 
Využít lze nejen počítač, ale i chytrý telefon či tablet.
Není třeba žádné instalace, stačí vstoupit na příslušné webové stránky a mít 
potřebnou znalost přihlašovacích údajů. Stránky jsou optimalizovány pro použití 
na rozdílných velikostech displeje, proto budou vhodně zobrazeny na počítači i 
na mobilním telefonu.